from os import environ
import json
from extended_alta import ExtendedALTA


def get_taskid_data(task_id):
    """Get the data from the output_fields for a taskID, combining the relevant sources in ALTA and write it to a json document.
    The data comes from activities, observations and dataproducts so those are all queried and combined before returning
    the specified fields. The output json file is named like the MS but with the extension .json in stead of .MS

    Parameters
    ----------
    task_id : str
        taskID to get tbe data for.
    """
    alta_interface = ExtendedALTA()

    output_fields = [
        "dataProductType",
        "dataProductSubType",
        "RA",
        "dec",
        "fov",
        "name",
        "beam",
        "datasetID",
        "target",
        "startTime",
        "endTime",
        "derived_duration",
        "lo",
        "sub1",
        "equinox",
        "bandwidth",
        "quality",
    ]

    dataproducts_table = (
        "dataproducts-flat"  # select dataProductSubType and generatedByActivity__runId
    )
    observations_table = "observations-flat"

    data_product_sub_type = "uncalibratedVisibility"
    activity_table = "activities-flat"  # select on runId

    alta_interface.init_json_iterator(
        activity_table,
        query=f"runId={task_id}",
    )  # first query: find the activity data corresponding to the task_id

    for result in alta_interface:
        for result_entry in result["results"]:
            activity = result_entry

    alta_interface.init_json_iterator(
        observations_table,
        query=f"runId={task_id}",
    )  # second query: find the observation data corresponding to the task_id

    for result in alta_interface:
        for result_entry in result["results"]:
            obs = result_entry

    observation_and_activity = (
        obs | activity
    )  # The obscure notation of dict concatenation.

    alta_interface.init_json_iterator(
        dataproducts_table,
        query=f"dataProductSubType={data_product_sub_type}&generatedByActivity__runId={task_id}",
    )  # third query: query data products created by the task_id

    dataproducts = list()

    for result in alta_interface:
        for result_entry in result["results"]:
            output_data = (
                observation_and_activity | result_entry
            )  # The obscure notation of dict concatenation.
            output_data["beam"] = int(
                output_data["name"].split("_")[1].split(".")[0].lstrip("B")
            )  # Take beam number from file name
            dataproducts.append(
                {outkey: output_data[outkey] for outkey in output_fields}
            )

    # WSRTA190807041_B000.MS -> raw_190807041_B036.json
    for dp in dataproducts:
        outfilename = dp["name"].replace("MS", "json").replace("WSRTA", "raw_")
        with open(outfilename, "w") as ofh:
            ofh.write(json.dumps(dp))


def main():
    with open("dr1_taskidlist") as tifh:
        tiddata = tifh.readlines()

    for taskid_line in tiddata:
        taskid, rawcount = taskid_line.split()
        if int(rawcount) == 0:
            get_taskid_data(taskid)


if __name__ == "__main__":
    main()
