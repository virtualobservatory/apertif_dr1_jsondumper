# apertif_dr1_jsondumper

Script used to obtain the json files corresponding to the visibility-only data from APERTIF DR1. This script also is a demonstrator of how to use the extended ALTA library to quey the ALTA REST API.

## Getting started
To run thios script, please first install the extended-alta library as indicated in the README of its repo:

https://git.astron.nl/astron-sdc/extended-alta

Then you can simply clone this repo and eecute the `get_raw_data.py` script.
